package ch.bfh.superbfh;

public class TrainData {
    public String production;
    public String time;

    public TrainData() {

    }

    public TrainData(String production, String time) {
        this.production = production;
        this.time = time;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}

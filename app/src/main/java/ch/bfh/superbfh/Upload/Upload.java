package ch.bfh.superbfh.Upload;

public class Upload {

    public String name;
    public String url;

    // Default constructor
    public Upload() {
    }

    public Upload(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}